# renarration-web

Re-narration of the Web using W3C Annotations.

A decentralized wikipedia of sorts where documents on the Web are available for local dissemination and open to "edits" to help create alternative versions that are more suitable for local consumption. 